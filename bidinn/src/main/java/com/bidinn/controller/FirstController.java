package com.bidinn.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bidinn.entity.Student;
import com.bidinn.service.StudentServices;

@RestController
public class FirstController {
	@Autowired
	private StudentServices services;
	
	@GetMapping("/Student")
	public List<Student> getStudents() {
		return this.services.getStudents();
	}
	
	@GetMapping("/Student/{studentId}")
	public Student getStudent(@PathVariable String studentId) {
		return this.services.getStudentById(Long.parseLong(studentId));
	}
	
	@PostMapping(path = "/Student")
	public Student addStudent(@RequestBody Student student) {
		return this.services.addStudent(student);
	}
	
	@PutMapping(path = "/Student")
	public Student updateStudent(@RequestBody Student student) {
		return this.services.updateStudent(student);
	}
	@DeleteMapping(path = "Student/{studentId}")
	public void deleteStudent(@PathVariable String studentId) {
		services.deleteStudentById(Long.parseLong(studentId)); 
	}
}

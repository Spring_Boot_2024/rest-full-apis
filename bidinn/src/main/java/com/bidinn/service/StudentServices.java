package com.bidinn.service;

import java.util.List;

import com.bidinn.entity.Student;

public interface StudentServices {
	public List<Student> getStudents();

	public Student getStudentById(long studentId);

	public Student addStudent(Student student);

	public Student updateStudent(Student student);

	public void deleteStudentById(long studentId);
}

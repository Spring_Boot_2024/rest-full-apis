package com.bidinn.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bidinn.dao.StudentDao;
import com.bidinn.entity.Student;

@Service
public class StudentServicesImpl implements StudentServices {
	 List<Student> students;
	
	@Autowired
	public StudentDao dao;
	
	public StudentServicesImpl() {
		/*
		 * students = new ArrayList<>(); students.add(new Student(120,
		 * "Lokesh Singh Rajwar",45,263139)); students.add(new Student(121,
		 * "Puran Singh Rajwar",37,145237)); students.add(new Student(122,
		 * "Ganga Rajwar",95,159847));
		 */
	}
	
	@Override
	public List<Student> getStudents() {
		return dao.findAll();
	}

	@Override
	public Student getStudentById(long studentId) {
		/*
		 * Student s = null; for(Student stu: students) { if(stu.getId() == studentId) {
		 * s = stu; break; } } return s;
		 */
        return dao.findById((int)studentId).get();
	}

	@Override
	public Student addStudent(Student student) {
		/*
		 * for (Student existingStudent : students) { if (existingStudent.getId() ==
		 * student.getId()) {
		 * System.out.println("Student with the same ID already exists: " +
		 * student.getId()); return null; } } students.add(student);
		 * System.out.println("Student added successfully: " + student.getId()); return
		 * student;
		 */
		dao.save(student);
		return student;
	}


	@Override
	public Student updateStudent(Student student) {
		/*
		 * students.forEach(stu -> { if(stu.getId() == student.getId()) {
		 * stu.setName(student.getName()); stu.setRollNo(student.getRollNo());
		 * stu.setContact(student.getContact()); } }); return student;
		 */	
		dao.save(student);
		return student;
	}

	@Override
	public void deleteStudentById(long studentId) {
		/*
		 * Iterator<Student> iterator = students.iterator(); while (iterator.hasNext())
		 * { Student stu = iterator.next(); if (stu.getId() == studentId) {
		 * iterator.remove(); System.out.println("Student deleted successfully: " +
		 * stu.getId()); } }
		 */
		dao.deleteById((int) studentId);
	}

	
}

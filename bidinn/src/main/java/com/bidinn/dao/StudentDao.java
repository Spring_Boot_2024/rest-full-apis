package com.bidinn.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bidinn.entity.Student;

public interface StudentDao extends JpaRepository<Student, Integer>{

}

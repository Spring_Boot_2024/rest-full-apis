package com.springboot.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;


import com.springboot.entity.Hotel;

public interface HotelDao extends JpaRepository<Hotel, Integer>{
    List<Hotel> findByHotelName(String hotelName);
}
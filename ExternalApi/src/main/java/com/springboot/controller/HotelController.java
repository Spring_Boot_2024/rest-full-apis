package com.springboot.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import com.springboot.entity.Hotel;
import com.springboot.service.HotelServices;


@RestController
public class HotelController {
		@Autowired	
		private HotelServices services;
		
		@GetMapping(value = "/hotels",produces = MediaType.APPLICATION_JSON_VALUE)
		public @ResponseBody List<Hotel> getHotels(){
			List<Hotel> list = services.getHotels();
		return list;
		}
		
		@GetMapping(value = "/hotels/{name}",produces = MediaType.APPLICATION_JSON_VALUE)
		public @ResponseBody List<Hotel> getHotelsByName(@PathVariable("name") String hotelName){
			List<Hotel> list = new ArrayList<Hotel>();
			list = services.getHotelByName(hotelName);
			return list;
		}
		
		@PostMapping(value = "/addhotel",produces = MediaType.APPLICATION_JSON_VALUE,consumes = MediaType.APPLICATION_JSON_VALUE)
		public boolean addHotel(@RequestBody Hotel hotel) {
			boolean isAdded = false;
			isAdded = services.addHotel(hotel);
			return isAdded;
		}
		
		@PatchMapping(value = "/edithotels",produces = MediaType.APPLICATION_JSON_VALUE,consumes = MediaType.APPLICATION_JSON_VALUE)
		public boolean updateHotel(@RequestBody Hotel hotel) {
			boolean isUpdated = false;
			isUpdated = services.updateHotel(hotel);
			return isUpdated;
		}
		
		@DeleteMapping(value = "/removehotels/{hotelId}",produces = MediaType.APPLICATION_JSON_VALUE)
		public boolean deleteHotel(@PathVariable int hotelId) {
			boolean isDeleted = false;
			isDeleted = services.deleteHotel(hotelId);
			return isDeleted;
		}
		
}

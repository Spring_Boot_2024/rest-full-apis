package com.springboot.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class Hotel {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int hotelId;
	@Column(name = "HotelName")
	private String hotelName;
	private String hotelType;
	private int hotelRating;
	private String address;
	private String city;
	private String postalCode;
	private String country;
	private String contactNumber;
	private String openTime;
	private String closeTime;
	
	public Hotel() {	
	}
	
	public Hotel(String hotelName, String hotelType, int hotelRating, String address, String city,
			String postalCode, String country, String contactNumber, String openTime, String closeTime) {
		super();
		this.hotelName = hotelName;
		this.hotelType = hotelType;
		this.hotelRating = hotelRating;
		this.address = address;
		this.city = city;
		this.postalCode = postalCode;
		this.country = country;
		this.contactNumber = contactNumber;
		this.openTime = openTime;
		this.closeTime = closeTime;
	}
	public int getHotelId() {
		return hotelId;
	}
	public String getHotelName() {
		return hotelName;
	}
	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}
	public String getHotelType() {
		return hotelType;
	}
	public void setHotelaaType(String hotelType) {
		this.hotelType = hotelType;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	public int getHotelRating() {
		return hotelRating;
	}
	public void setHotelRating(int hotelRating) {
		this.hotelRating = hotelRating;
	}
	public String getOpenTime() {
		return openTime;
	}
	public void setOpenTime(String openTime) {
		this.openTime = openTime;
	}
	public String getCloseTime() {
		return closeTime;
	}
	public void setCloseTime(String closeTime) {
		this.closeTime = closeTime;
	}
	
	@Override
	public String toString() {
		return "Hotel [hotelId=" + hotelId + ", hotelName=" + hotelName + ", hotelType=" + hotelType + ", hotelRating="
				+ hotelRating + ", address=" + address + ", city=" + city + ", postalCode=" + postalCode + ", country="
				+ country + ", contactNumber=" + contactNumber + ", openTime=" + openTime + ", closeTime=" + closeTime
				+ "]";
	}

}
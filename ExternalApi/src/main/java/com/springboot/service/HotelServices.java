package com.springboot.service;

import java.util.List;

import com.springboot.entity.Hotel;

public interface HotelServices {
	
	public List<Hotel> getHotels();
	
	public List<Hotel> getHotelByName(String hotelName);
	
	public boolean addHotel(Hotel hotel);
	
	public boolean updateHotel(Hotel hotel);
	
	public boolean deleteHotel(int hotelId);
	
}

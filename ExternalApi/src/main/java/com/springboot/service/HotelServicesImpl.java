package com.springboot.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.dao.HotelDao;
import com.springboot.entity.Hotel;

@Service
public class HotelServicesImpl implements HotelServices{
	
	@Autowired
	private HotelDao dao;
	
	@Override
	public List<Hotel> getHotels() {
		List<Hotel> list = dao.findAll();
		return list;
	}

	@Override
	public List<Hotel> getHotelByName(String hotelName) {
		List<Hotel> list = dao.findByHotelName(hotelName);
		return list;
	}

	@Override
	public boolean addHotel(Hotel hotel) {
		dao.save(hotel);
		return true;
	}

	@Override
	public boolean updateHotel(Hotel hotel) {
		dao.save(hotel);
		return true;
	}

	@Override
	public boolean deleteHotel(int hotelId) {
		long temp = dao.count();
		dao.deleteById(hotelId);
		if(temp != dao.count())
			return true;
		return false;
	}

	

}

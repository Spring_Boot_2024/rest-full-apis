package com.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class ApplicationClass {
	public static void main(String args[]) {
		System.out.println("Welcome to External API's !!!");
		ConfigurableApplicationContext context = SpringApplication.run(ApplicationClass.class, args);
		context.getBeanProvider(ApplicationClass.class, true);
	}
}
